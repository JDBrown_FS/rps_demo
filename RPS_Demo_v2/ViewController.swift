//
//  ViewController.swift
//  RPS_Demo_v2
//
//  Created by JamieBrown on 9/10/18.
//  Copyright © 2018 FullSailUniversity. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var playerChoiceImageView: UIImageView!
    @IBOutlet weak var opponentChoiceImageView: UIImageView!

    @IBOutlet weak var gamesUpdateLabel: UILabel!
    @IBOutlet var gameButtons: [UIButton]!
    @IBOutlet weak var playButton: UIButton!
    
    //Player & Opponent choices as Int
    var playerChoice:Int = 0
    var opponentChoice:Int = 0
    //CurrentTime in whole seconds. Used for 3..2..1 countdown
    var currentTime:Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        //Fix Button Image Aspects & add outlines
        for btn in gameButtons {
            btn.imageView!.contentMode = UIViewContentMode.scaleAspectFit
            btn.layer.borderWidth = 3
            btn.layer.cornerRadius = 10
        }
        //Disable Buttons until Play is pressed
        ToggleGameButtonInput(input: false)
        //Clear Out Game Board
        ClearGameBoard()
    }

    func ToggleGameButtonInput(input: Bool) {
        for btn in gameButtons {
            btn.isEnabled = input
        }
    }

    func ClearGameBoard() {
        playerChoiceImageView.image = nil
        opponentChoiceImageView.image = nil
    }


    //MARK: - GamePlay Methods
    func CPUChoice() {
        //Random Opponent Choice between 1 and 3
        opponentChoice = Int(arc4random_uniform(3) + 1)
        //opponentChoice = Int.random(in: 1...3)

        switch opponentChoice {
        case 1:
            opponentChoiceImageView.image = #imageLiteral(resourceName: "rock")
        case 2:
            opponentChoiceImageView.image = #imageLiteral(resourceName: "paper")
        case 3:
            opponentChoiceImageView.image = #imageLiteral(resourceName: "scissors")
        default:
            //We should never get here!
            assertionFailure()
        }

        //Resolve Round, Check Win,Lose,Draw
        ResolveRound()
    }

    func ResolveRound() {
        // Rock == 1     Paper == 2     Scissors == 3

        switch playerChoice - opponentChoice {
        case 0:
            gamesUpdateLabel.text = "Draw"
        case -1, 2:
            gamesUpdateLabel.text = "You Lose"
        case 1, -2:
            gamesUpdateLabel.text = "You Win"
        default:
            assertionFailure()
        }

        playButton.isEnabled = true
    }

    //MARK: - Input Methods & Helpers
    @IBAction func GameButtonPressed(_ sender: UIButton) {

        //Set Player's Choice (Int)
        playerChoice = sender.tag

        //Populate ImageView with Player's Choice
        switch sender.tag {
        case 1:
            playerChoiceImageView.image = #imageLiteral(resourceName: "rock")
        case 2:
            playerChoiceImageView.image = #imageLiteral(resourceName: "paper")
        case 3:
            playerChoiceImageView.image = #imageLiteral(resourceName: "scissors")
        default:
            assertionFailure()
        }
    }

    @IBAction func PlayButtonPressed(_ sender: UIButton) {

        //Disable Play Button
        playButton.isEnabled = false

        //Enable Game Buttons
        ToggleGameButtonInput(input: true)

        //Clear Game Board
        ClearGameBoard()

        //Update Timer to prepare for countdown
        currentTime = 3

        //Start & Update Timer for Countdown
        let timer = Timer.scheduledTimer(timeInterval: 1.0,
                                         target: self,
                                         selector: #selector(TimerTick),
                                         userInfo: nil,
                                         repeats: true)
        //Fire Timer Immediately
        timer.fire()
    }

    @objc
    func TimerTick(t: Timer) {
        //Countdown
        if currentTime > 0 {
            gamesUpdateLabel.text = "\(currentTime)..."
            currentTime -= 1
        }
            //Timer has finished
        else {
            //Stop Timer
            t.invalidate()

            //Disable Input
            ToggleGameButtonInput(input: false)

            //Make CPU Choice
            CPUChoice()
        }
    }
}

